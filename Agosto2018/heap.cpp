#include <iostream>
#include <vector>
using namespace std;

template <typename T>
struct Heap{
	vector<T> A;

	Heap() : A(1){}

	Heap( vector<T>& aux ){
		A.push_back(0);
		A.insert( A.end(), aux.begin(), aux.end() );
		for( int i = A.size()/2; i >= 1; i-- ){
			heapify( i );
		}
	}

	void heapify( int i ){
		int l = 2*i;
		int r = 2*i + 1;
		int largest = i;
		if( l < A.size() && A[l] > A[i] )
			largest = l;
		if( r < A.size() && A[r] > A[largest] )
			largest = r;
		if( largest != i ){
			swap( A[i], A[largest] );
			heapify( largest );
		}
	}

	T top(){
		if( A.size() <= 1 ) throw runtime_error( "Heap underflow" );
		return A[1];
	}

	void pop(){
		if( A.size() <= 1 ) throw runtime_error( "Heap underflow" );
		A[1] = A[A.size()-1];
		A.pop_back();
		heapify( 1 );
	}

	void push( T v ){
		A.push_back( v );
		int i = A.size()-1;
		while( i > 1 && A[i/2] < A[i] ){
			swap( A[i], A[i/2] );
			i = i/2;
		}
	}

	void print(){
		for( auto &it : A ){
			cout << it << " ";
		}
		cout << endl;
	}
};

int main(){
	vector<int> tmp = {1, 4, 2, 3, 9, 7, 8, 10, 14, 16};
	Heap<int> h( tmp );
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	h.push(100);
	h.push(102);
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	cout << h.top() << endl;
	h.pop();
	return 0;
}