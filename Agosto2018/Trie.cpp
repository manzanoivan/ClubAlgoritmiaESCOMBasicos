#include <iostream>
#include <unordered_map>
using namespace std;

struct Nodo{
	unordered_map <char, Nodo*> hijos;
	int fin, prefix;
	Nodo(): fin(0), prefix(0){}	
};

struct Trie{
	Nodo *raiz;

	Trie() : raiz( new Nodo() ){}

	void insert( string s ){
		Nodo* act= raiz;
		for (auto &c : s){
			if(!(act->hijos).count(c))
				act->hijos[c] = new Nodo();
			act = act->hijos[c];
			act->prefix++;
		}
		act->fin++;
	}

	int query(string s){
		Nodo* act = raiz;
		for( auto &c : s){
			if( !(act->hijos.count(c)) )
				return 0;
			act = act->hijos[c];
		}
		return act->fin;
	}
};

int main(){
	Trie t;
	t.insert( "crotolamo" );
	t.insert( "crotosobo" );
	t.insert( "carrera" );
	t.insert( "arbol" );
	t.insert( "rojo" );
	cout << t.query( "verde" ) << endl;
	cout << t.query( "rojo" ) << endl;
	cout << t.query( "azul" ) << endl;
	cout << t.query( "carrera" ) << endl;
	t.insert( "carrera" );
	cout << t.query( "carrera" ) << endl;

	return 0;
}